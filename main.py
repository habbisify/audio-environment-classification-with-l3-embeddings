# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 22:41:28 2019

@author: Joni Seppälä

This is my script for my bachelors thesis on DCASE 2019 CHALLENGE, using embeddings generated with L3.
It is also the script I used to generate my submission in the Kaggle competition

    https://www.kaggle.com/c/dcase2019-task1a-leaderboard

The script creates embeddings using openl3 if desired, and imports the generated or already existing embeddings.

After aquiring the embeddings it vectorizes them using the method chosen for the variable vect_method.
Truth labels for the embeddings are aquired from meta.csv.

The embeddings are split into train and test sets according to the fold1_train.csv and fold1_test.csv files.
Train set is further split into train and validation sets.

Either classic or neural network classifiers are trained with the embeddings, based on the variable clf_type.
Neural network's adjustable parameters are listed below and can be tuned. Early stopping is implemented.

After training predictions are generated. The training process and the confusion matrix are visualized.

If the variable kaggle is set to True, a submission (.csv) file is generated automatically.
"""
#%% used parameters

create_embeddings = False # should be False if the embeddings have already been generated
kaggle = True # if the Kaggle submission is to be generated

vect_method = "avgstd" # avg, avgstd, sqavgstd or raw
clf_type = "classic" # classic or default_NN

# Neural network and early stopping parameters
epochs = 1000 # how many iterations the NN is taught maximum, provided no early stopping
batch_size = 16 # how many data points are fed to the NN at a time - usually a power of 2
min_delta = 0 # minimum improvement over last epoch required
patience = 10 # amount of epochs that are allowed to fail the min_delta condition in a row
baseline = 0.6 # the minimum accuracy required to carry to the next epoch

#%% imports, path-specs and name-lists

from random import random, shuffle
from time import time

import numpy as np
import os

from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from sklearn import neighbors, svm, linear_model, discriminant_analysis
#from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, ExtraTreesClassifier, GradientBoostingClassifier

from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import EarlyStopping

import openl3
import soundfile as sf

from utils import plot_training_process, plot_confusion_matrix



path = 'path\\to\\dev\\folder'

patha = path + 'TAU-urban-acoustic-scenes-2019-development\\'
pathae = patha + 'evaluation_setup\\'

pathe = [path + 'embeddings\\env_mel256_512\\']
if kaggle:
    pathe.append(path + 'embeddings\\kaggle\\env_mel256_512\\')

pathsub = path + 'kaggle_submissions\\'



vect_compacts = ["avg","avgstd", "sqavgstd"]
NNs = ["default_NN"]
classics = ["all","NN1","NN1-5","LDA","SVM"]

#%% create embeddings

if create_embeddings:

    model = openl3.models.load_embedding_model(input_repr="linear", content_type="env",
                                               embedding_size=512)
    for file in [f for f in os.listdir(patha) if f.endswith('.wav')]:
        
        audio, sr = sf.read(patha + file)
        emb, ts = openl3.get_embedding(audio, sr, model=model)
        
        np.save(file[:-4],emb)

#%% import embeddings
    
embs = []
embs_kaggle = []

print("Importing embeddings...")

for i,p in enumerate(pathe):
    t=0
    
    npys = [f for f in os.listdir(p) if f.endswith('.npy')]
    
    if i == 1:
        #sort Kaggle embeddings by numerical order
        npys = [int(npy[:-4]) for npy in npys]
        npys.sort()
        npys = [str(npy)+".npy" for npy in npys]
    
    for file in npys:
        
        emb = np.load(p + file)
        
        if i == 0:
            embs.append(emb)
        else:
            embs_kaggle.append(emb)
            
        if t % (int(len(npys)/10)) == 0:
            progress_amt = int(t*10.0/len(npys))
            print("["+progress_amt*"#"+">"+(9-progress_amt)*"_"+"]")
                    
        t+=1

print("Done importing embeddings\n")
    
#%% vectorize embeddings

print("Vectorizing embeddings...")

Xs = []
Xs_i = []
X_kaggle = []
Xi_kaggle = []

for i,emb in enumerate(embs):
    if vect_method in vect_compacts:
        
        if vect_method == "sqavgstd":
            # square the values of the embedding
            emb = emb**2
        # average over time axis
        x = np.mean(emb,0)
        if vect_method == "avgstd":
            # average and standard deviation over time axis
            x_std = np.std(emb,0)
            x = np.concatenate((x,x_std))
            
        Xs.append(x)
    
    if vect_method == "raw":
        for t in emb:
            Xs.append(t)
            Xs_i.append(i)

if kaggle:
    for i,emb in enumerate(embs_kaggle):
        if vect_method in vect_compacts:
            
            if vect_method == "sqavgstd":
                # square the values of the embedding
                emb = emb**2
            # average over time axis
            x = np.mean(emb,0)
            if vect_method == "avgstd":
                # average and standard deviation over time axis
                x_std = np.std(emb,0)
                x = np.concatenate((x,x_std))
                
            X_kaggle.append(x)
        
        if vect_method == "raw":
            for t in emb:
                X_kaggle.append(t)
                Xi_kaggle.append(i)

Xs = np.asarray(Xs)
Xs_i = np.asarray(Xs_i)
X_kaggle = np.asarray(X_kaggle)
Xi_kaggle = np.asarray(Xi_kaggle)

print("Done vectorizing embeddings\n")

#%% get truth labels for embeddings

y_data = np.genfromtxt(patha+"meta.csv",dtype=str)[1:]

# sort the data to match the embeddings
y_data = y_data[y_data[:, 1].argsort()]

# get the truth labels
y = [el[1] for el in y_data]

# encode the truth labels
le = preprocessing.LabelEncoder()
le.fit(y)
y = le.transform(y)

# create str representations of the class names
class_names = le.inverse_transform(list(range(10)))

#%% export fold used

fold_train = np.genfromtxt(pathae+"fold1_train.csv",dtype=str)
fold_test = np.genfromtxt(pathae+"fold1_test.csv",dtype=str)

# export group_ids
group_ids = set()
for i,d in enumerate(y_data):
    y_name = d[0]
    if not y_name in fold_test:
        group_id = d[2]
        group_ids.add(str(group_id))
group_ids = list(group_ids)
shuffle(group_ids)

# split group_ids to train (70%) and validation (30%)
train_ids = []
validation_ids = []
for group_id in group_ids:
    if random()>0.3:
        train_ids.append(str(group_id))
    else:
        validation_ids.append(str(group_id))

# create fold for train (0), validation (1) and test (2)
y_fold = list(y)
for i,d in enumerate(y_data):
    y_name = d[0]
    if y_name in fold_test:
        y_fold[i] = 2
    else:
        group_id = d[2]
        if group_id in train_ids:
            y_fold[i] = 0
        else:
            # in validation_ids
            y_fold[i] = 1

#%% split to train, validation and test

print("Splitting to train/validation/test...")

X_train = []
X_val = []
X_test = []
y_train = []
y_val = []
y_test = []
y_test_true = []
y_test_kaggle = []

for i,f in enumerate(y_fold):
    
    # train
    if f == 0:
        if vect_method in vect_compacts:
            X_train.append(Xs[i])
            y_train.append(y[i])
        elif vect_method == "raw":
            indexes = np.where(Xs_i == i)[0]
            for j in indexes:
                X_train.append(Xs[j])
                y_train.append(y[i])
        
    # validation        
    elif f == 1:
        if vect_method in vect_compacts:
            X_val.append(Xs[i])
            y_val.append(y[i])
        elif vect_method == "raw":
            indexes = np.where(Xs_i == i)[0]
            for j in indexes:
                X_val.append(Xs[j])
                y_val.append(y[i])
   
    # test             
    else:
        if vect_method in vect_compacts:
            X_test.append(Xs[i])
            y_test.append(y[i])
        elif vect_method == "raw":
            indexes = np.where(Xs_i == i)[0]
            for j in indexes:
                X_test.append(Xs[j])
                y_test.append(y[i])
            y_test_true.append([y[i],int(len(indexes))])

# kaggle test
if kaggle:
    for i in range(1200):
        X_kaggle_count = len(np.where(Xi_kaggle == i)[0])
        y_test_kaggle.append(int(X_kaggle_count))

X_train = np.asarray(X_train)
X_val = np.asarray(X_val)
X_test = np.asarray(X_test)
y_train = np.asarray(y_train)
y_val = np.asarray(y_val)
y_test = np.asarray(y_test)
y_test_true = np.asarray(y_test_true)
y_test_kaggle = np.asarray(y_test_kaggle)

'''
# The following simple method doesn't take group id into account and therefore gives ~20% too good results with the classifiers
X_train,X_test,y_train,y_test = model_selection.train_test_split(Xs,y_train,test_size=0.3)
'''

print("Done splitting to train/validation/test\n")

#%% train classical machine learning classifiers

if clf_type == "classic":

    classifiers = [#discriminant_analysis.LinearDiscriminantAnalysis(),
                   neighbors.KNeighborsClassifier(n_neighbors=1),
                   #neighbors.KNeighborsClassifier(n_neighbors=5),
                   #svm.SVC(kernel="linear"),
                   #svm.SVC(kernel="rbf",gamma="auto"),
                   #linear_model.LogisticRegression(solver="lbfgs", multi_class="multinomial",max_iter=2000),
                   #AdaBoostClassifier(),
                   #ExtraTreesClassifier(n_estimators=1000),
                   #RandomForestClassifier(n_estimators=1000),
                   #GradientBoostingClassifier()
                   ]
    
    clf_names = [#"LDA",
                 "NN1",
                 #"NN5",
                 #"lin SVC",
                 #"RBF SVC",
                 #"logreg",
                 #"AdaBoost",
                 #"Extra Trees",
                 #"RF",
                 #"GB-Trees"
                 ]
    
    for i, clf in enumerate(classifiers):
        t1 = time()
        print("Training",clf_names[i],"...")
        clf.fit(X_train,y_train)
        
        if vect_method in vect_compacts:
            
            y_preds = clf.predict(X_test)
            
            accuracy = accuracy_score(y_test, y_preds)
            print(clf_names[i],": %.3f%%" % (100 * accuracy))
            print("Time taken",time() - t1,"s")
            
            # predict true labels for kaggle   
            if kaggle:
                y_preds_kaggle = clf.predict(X_kaggle)


        if vect_method == "raw":
            
            # using majority vote
            y_preds = []
            start = 0
            for y_t in y_test_true:
                y_pred = clf.predict(X_test[start:(start+y_t[1])])
                
                counts = np.bincount(y_pred)
                y_preds.append(np.argmax(counts))
                
                start += y_t[1]
            
            accuracy = accuracy_score([y_t[0] for y_t in y_test_true], y_preds)
            print(clf_names[i],": %.3f%%" % (100 * accuracy))
            print("Time taken",time() - t1,"s")

#%% train neural networks

if clf_type == "default_NN":
    
    # construct model
    model_name = "default MLP"
    
    model = Sequential()
    model.add(Dense(512, input_dim=len(X_train[0]), kernel_initializer='uniform', activation='relu'))
    model.add(Dense(128, kernel_initializer='uniform', activation='relu'))
    model.add(Dense(10, kernel_initializer='uniform', activation='softmax'))
    
    '''
    # load weights
    model.load_weights("weights.best.hdf5")
    '''
    
    # compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    
    # transform label data to dimensions (N, m)
    enc = preprocessing.OneHotEncoder(categories='auto')
    y_train_2d = np.reshape(y_train,(-1,1))
    y_val_2d = np.reshape(y_val,(-1,1))
    enc.fit(y_train_2d)
    y_train_n = enc.transform(y_train_2d)
    y_val_n = enc.transform(y_val_2d)
    
    # create checkpoints
    es = EarlyStopping(monitor='acc', min_delta=min_delta, patience=patience, verbose=1, mode='auto', baseline=baseline, restore_best_weights=True)
    
    t1 = time()
    history = model.fit(X_train, y_train_n, epochs=epochs, batch_size=batch_size, verbose=2, validation_data=[X_val,y_val_n], callbacks=[es])
    
    #%% predict true labels with neural networks
    
    print("\nCreating predictions...")
    
    if vect_method in vect_compacts:
    
        y_pred_n = model.predict(X_test)
        y_preds = enc.inverse_transform(y_pred_n)
        
        # predict true labels for kaggle   
        if kaggle:
            y_pred_kaggle_n = model.predict(X_kaggle)
            y_preds_kaggle = enc.inverse_transform(y_pred_kaggle_n)
    
    if vect_method == "raw":
        
        # using majority vote
        y_preds = []
        start = 0
        for y_t in y_test_true:
            y_pred_n = model.predict(X_test[start:(start+y_t[1])])
            
            y_pred = enc.inverse_transform(y_pred_n)
            y_pred = y_pred.flatten()
            counts = np.bincount(y_pred)
            y_preds.append(np.argmax(counts))
            
            start += y_t[1]
            
        y_test = [y_t[0] for y_t in y_test_true]
        
        # predict true labels for kaggle with majority vote 
        if kaggle:
            y_preds_kaggle = []
            start = 0
            for y_t in y_test_kaggle:
                y_pred_n = model.predict(X_kaggle[start:(start+y_t)])
                
                y_pred = enc.inverse_transform(y_pred_n)
                y_pred = y_pred.flatten()
                counts = np.bincount(y_pred)
                y_preds_kaggle.append(np.argmax(counts))
                
                start += y_t
        
    accuracy = accuracy_score(y_test, y_preds)
    
    print(model_name,": %.3f%%" % (100 * accuracy))
    print("Time taken",time() - t1,"s")
        
    #%% visualize the training process

    plot_training_process(history)
    
#%% Plot normalized confusion matrix

plot_confusion_matrix(y_test, y_preds, classes=class_names, normalize=True,
                      title='Normalized confusion matrix')

#%% generate Kaggle submission file

if kaggle:
    
    labels = list(le.inverse_transform(y_preds_kaggle))
    
    os.chdir(pathsub)
    
    name = vect_method+"-"+clf_type
    if clf_type in NNs:
        name += "b"+str(batch_size)
        name += "e"+str(len(history.epoch))
        name += "p"+str(patience)
    name += "-"+str(accuracy/10)[3:7]+".csv"
    
    with open(name, "w") as fp:
        fp.write("Id,Scene_label\n")
        for i, label in enumerate(labels):
            #print (str(label))
            fp.write("%d,%s\n" % (i, str(label)))










